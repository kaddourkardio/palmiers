# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-26 22:21
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='certificat',
            name='certificat_date',
            field=models.DateField(default=datetime.datetime(2016, 11, 26, 22, 21, 16, 639532, tzinfo=utc), verbose_name='Date'),
        ),
    ]
