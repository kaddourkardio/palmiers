# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-28 21:53
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20161128_2253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='certificat',
            name='certificat_date',
            field=models.DateField(default=datetime.datetime(2016, 11, 28, 21, 53, 48, 107273, tzinfo=utc), verbose_name='Date'),
        ),
    ]
