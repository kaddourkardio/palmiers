[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)


# Palmiers
**Palmiers** is a web application for management of a cardiology departement' patients. It is built with [Python][0] using the [Django Web Framework][1].

This project has the following basic apps:
- app : for recording medical reports
- interventions: for reporting several interventions (angioplasty, pacing...)
 

 
 
### aknowledgements

[Arun Ravindran](http://arunrocks.com) for his django template (edge).

http://django-edge.readthedocs.org/

[0]: https://www.python.org/
[1]: https://www.djangoproject.com/
