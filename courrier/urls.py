from django.conf.urls import url, include
from django.conf import settings
from .views import courrier_pdf, arret_pdf, courrier_easy
from .models import Courrier, Certificat, Arret, Stomato


urlpatterns = [
    url(r'patient(?P<pk1>\d+)/courrier(?P<pk2>\d+)\.pdf$', courrier_pdf, name='courrier_pdf'),
    url(r'patient(?P<pk1>\d+)_courrier(?P<pk2>\d+)\.pdf$', courrier_easy, name='courrier_easy'),
    url(r'patient(?P<pk1>\d+)/arret(?P<pk2>\d+)\.pdf$', arret_pdf, name='arret_pdf'),
]

