(TeX-add-style-hook
 "arret"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrartcl" "headlines=6" "headinclude=true" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "left=1.5cm" "right=1.5cm" "top=4cm" "bottom=2cm" "includefoot") ("xcolor" "usenames" "dvipsnames" "svgnames" "table") ("tcolorbox" "skins")))
   (TeX-run-style-hooks
    "latex2e"
    "scrartcl"
    "scrartcl11"
    "scrlayer-scrpage"
    "geometry"
    "xcolor"
    "graphicx"
    "tcolorbox"
    "titlesec"
    "longtable"
    "booktabs"
    "fontawesome"
    "ifthen"
    "polyglossia")
   (LaTeX-add-polyglossia-langs
    '("french" "defaultlanguage" "")
    '("arabic" "otherlanguage" "")))
 :latex)

