(TeX-add-style-hook
 "template"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ragged2e" "document") ("microtype" "protrusion=true" "final") ("hyperref" "xetex" "bookmarks" "colorlinks" "breaklinks")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "fontspec"
    "geometry"
    "ragged2e"
    "enumitem"
    "wallpaper"
    "polyglossia"
    "xunicode"
    "xltxtra"
    "microtype"
    "hyperref")
   (TeX-add-symbols
    "amper"
    "tightlist")
   (LaTeX-add-polyglossia-langs
    '("french" "mainlanguage" "")))
 :latex)

